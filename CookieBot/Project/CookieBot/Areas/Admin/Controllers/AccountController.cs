﻿using CookieBot.Context;
using CookieBot.Models;
using CookieBot.ViewModels;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using Microsoft.AspNet.Identity.Owin;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;

namespace CookieBot.Areas.Admin.Controllers
{
    public class AccountController : Controller
    {
        CookieContext _context = new CookieContext();
        private readonly UserManager<IdentityUser> userManager;
        public AccountController()
        {

        }
        public AccountController(UserManager<IdentityUser> userManager)
        {
            this.userManager = userManager;
        }
    
        public ActionResult Login()
        {
            var model = new VmUserLogin();
            return View(model);
        }

        [HttpPost]
        public ActionResult Login(VmUserLogin model)
        {
            if (ModelState.IsValid) 
            {
                try
                {
                    bool isValidUser = _context.AppUsers.Any(a => a.FullName == model.FullName && a.Password == model.Password);
                    if (isValidUser)
                    {
                        FormsAuthentication.SetAuthCookie(model.FullName, false);
                        return RedirectToAction("Index", "Manage");
                    }
                    else 
                    {
                        TempData["ErrorMessage"] = "Invalid User Name and Password!";
                    }
                }
                catch (Exception ex) 
                {
                    var errMsg = ex.Message;
                }
            }
            return View(model);
        }
        public ActionResult Register()
        {
            return View();
        }

        [HttpPost]
        public  ActionResult Register(Users model)
        {
            if (ModelState.IsValid)
            {

                try 
                {
                    if (_context.Users.Any(a => a.Email == model.Email))
                    {
                        TempData["ErrorMessage"] = "Sorry! the email address is already exists.";
                    }
                    else if (_context.AppUsers.Any(a => a.FullName == model.FullName))
                    {
                        TempData["ErrorMessage"] = "Sorry! the user name is already exists.";
                    }
                    else if (_context.Users.Any(a => a.PhoneNumber == model.PhoneNumber)) 
                    {
                        TempData["ErrorMessage"] = "Sorry! the phone number is already exists.";
                    }

                    else
                    {
                        
                        _context.AppUsers.Add(model);
                        _context.SaveChanges();
                        return RedirectToAction("Login");
                        //var user = new IdentityUser() { UserName = model.FullName, Email = model.Email, PhoneNumber = model.PhoneNumber };

                        //var result = userManager.Create(user, model.Password);
                        //if (result.Succeeded)
                        //{
                        //    _context.SaveChanges();
                        //    return RedirectToAction("Login");

                        //}

                    }
                }
                catch(Exception ex) 
                {
                    var exmsg = ex.Message;
                }
            }
            return View();
        }


        public ActionResult Logout() 
        {
            FormsAuthentication.SignOut();
            return RedirectToAction("Login");
        }
    }
}