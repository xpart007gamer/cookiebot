﻿using CookieBot.Areas.Admin.Services;
using CookieBot.Context;
using CookieBot.Models;
using CookieBot.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Security.Principal;
using Microsoft.AspNet.Identity;

namespace CookieBot.Areas.Admin.Controllers
{
    [Authorize]
    public class ManageController : Controller
    {
        CookieContext _context = new CookieContext();
      
        // GET: Admin/Manage
        public ActionResult Index()
        {
            var userName = User.Identity.Name;
     
           
            var domain = _context.Domains.FirstOrDefault();
            var content = _context.Contents.FirstOrDefault();
            var dialog = _context.Dialogs.FirstOrDefault();
            var script = _context.Scripts.FirstOrDefault();
            var model = new VmManage();
            if (domain == null && content == null && dialog == null && script == null)
            {
                var PrefilledContent = new Content
                {

                    DialogBody = "We use cookies to personalise content and ads, to provide social media features and to analyse our traffic. We also share information about your use of our site with our social media, advertising and analytics partners who may combine it with other information that you’ve provided to them or that they’ve collected from your use of their services.",
                    DialogHeading = "This website uses cookies",
                    Language = "English",
                    TextInDeclineButton = "Use necessary cookies only",
                    TextInOkButton = "Ok",
                    TextInAllowAllButton = "Allow all cookies",
                    TextInAllowSectionButton = "Allow selection",
                    TextInShowSectionButton = "Show details",
                    TextInHideDetailsButton = "Hide details",
                    GeneralCookieIntroduction = "Cookies are small text files that can be used by websites to make a user's experience more efficient." +
                    "" +
                    "" +
                    "The law states that we can store cookies on your device if they are strictly necessary for the operation of this site.For all other types of cookies we need your permission." +
                    "" +
                    "" +
                    "This site uses different types of cookies.Some cookies are placed by third party services that appear on our pages." +
                    "" +
                    "" +
                    "You can at any time change or withdraw your consent from the Cookie Declaration on our website." +
                    "" +
                    "Learn more about who we are, how you can contact us and how we process personal data in our Privacy Policy. " +
                    "" +
                    "   Please state your consent ID and date when you contact us regarding your consent.",
                    NecessaryCategoryTitle = "Necessary",
                    NecessaryCookieIntroduction = "Necessary cookies help make a website usable by enabling basic functions like page navigation and access to secure areas of the website. The website cannot function properly without these cookies.",
                    PreferenceCategoryTitle = "Preferences",
                    PreferenceCookieIntroduction = "Preference cookies enable a website to remember information that changes the way the website behaves or looks, like your preferred language or the region that you are in.",

                    StatisticCategoryTitle = "Statistics",
                    StatisticCookieIntroduction = "Statistic cookies help website owners to understand how visitors interact with websites by collecting and reporting information anonymously.",

                    MarketingCategoryTitle = "Marketing",
                    MarketingCookieIntroduction = "Marketing cookies are used to track visitors across websites. The intention is to display ads that are relevant and engaging for the individual user and thereby more valuable for publishers and third party advertisers.",

                    UnclassifiedCategoryTitle = "Unclassified",
                    UnclassifiedCookieIntroduction = "Unclassified cookies are cookies that we are in the process of classifying, together with the providers of individual cookies.",




                };
                var defaultDialogSetting = new Dialog
                {
                    MethodType = Enums.EnumSets.MethodType.Explicit_Consent,
                    PositionType = Enums.EnumSets.PositionType.Slide_Up,
                    ButtonType = Enums.EnumSets.ButtonType.Multilevel,
                    Button = Enums.EnumSets.Button.RejectAll_Selection_AllowAll,
                    PageScroll = true
                    

                };
                var defaultScript = new Script
                {
                    ScriptString = "<script src='https:/localhost:44380/Content/js/CallCookieBot.js' data-token=''></script>",
                    
                };
                var ViewModel = new VmManage
                {
                    Content = PrefilledContent,
                    Dialog = defaultDialogSetting,
                    Script = defaultScript
                };

                return View(ViewModel);



            }
            else
            {
                model.Content = content;
                model.Domain = domain;
                model.Dialog = dialog;
                model.Script = script;
            }

            return View(model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(VmManage model) 
        {
            var errors = ModelState.Values.SelectMany(v => v.Errors);
            if (ModelState.IsValid) 
            {
                try
                {
                    if (model.Content.Id > 0 && model.Domain.Id > 0 && model.Dialog.Id > 0 && model.Script.Id > 0) 
                    {
                        var domain = _context.Domains.FirstOrDefault(f => f.Id == model.Domain.Id);
                        var content = _context.Contents.FirstOrDefault(f => f.Id == model.Content.Id);
                        var dialog = _context.Dialogs.FirstOrDefault(f => f.Id == model.Dialog.Id);
                        var script = _context.Scripts.FirstOrDefault(f => f.Id == model.Script.Id);

                        domain.DomainName = model.Domain.DomainName;
                        domain.SchemeType = model.Domain.SchemeType;

                        content.DialogHeading = model.Content.DialogHeading;
                        content.DialogBody = model.Content.DialogBody;
                        content.Language = model.Content.Language;
                        content.TextInDeclineButton = model.Content.TextInDeclineButton;
                        content.TextInOkButton = model.Content.TextInOkButton;
                        content.TextInAllowAllButton = model.Content.TextInAllowAllButton;
                        content.TextInAllowSectionButton = model.Content.TextInAllowSectionButton;
                        content.TextInShowSectionButton = model.Content.TextInShowSectionButton;
                        content.TextInHideDetailsButton = model.Content.TextInHideDetailsButton;
                        content.GeneralCookieIntroduction = model.Content.GeneralCookieIntroduction;
                        content.NecessaryCategoryTitle = model.Content.NecessaryCategoryTitle;
                        content.NecessaryCookieIntroduction = model.Content.NecessaryCookieIntroduction;
                        content.PreferenceCategoryTitle = model.Content.PreferenceCategoryTitle;
                        content.StatisticCategoryTitle = model.Content.StatisticCategoryTitle;
                        content.StatisticCookieIntroduction = model.Content.StatisticCookieIntroduction;
                        content.MarketingCategoryTitle = model.Content.MarketingCategoryTitle;
                        content.MarketingCookieIntroduction = model.Content.MarketingCookieIntroduction;
                        content.UnclassifiedCategoryTitle = model.Content.UnclassifiedCategoryTitle;
                        content.UnclassifiedCookieIntroduction = model.Content.UnclassifiedCookieIntroduction;


                        dialog.TemplateType = model.Dialog.TemplateType;
                        dialog.MethodType = model.Dialog.MethodType;
                        dialog.PositionType = model.Dialog.PositionType;
                        dialog.ButtonType = model.Dialog.ButtonType;
                        dialog.Button = model.Dialog.Button;
                        dialog.Preference = model.Dialog.Preference;
                        dialog.Statistics = model.Dialog.Statistics;
                        dialog.Marketing = model.Dialog.Marketing;

                        script.ScriptString = model.Script.ScriptString;

                        _context.SaveChanges();

                    }
                    else
                    {
                        _context.Domains.Add(model.Domain);
                        _context.Dialogs.Add(model.Dialog);
                        _context.Contents.Add(model.Content);
                        _context.Scripts.Add(model.Script);

                        _context.SaveChanges();

                    }
                }
                catch (Exception e) 
                {
                    var errMsg = e.Message;
                }
            }

            return Json(JsonRequestBehavior.AllowGet);
        }
    }
}