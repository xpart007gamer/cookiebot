﻿$("#CybotCookiebotDialogBodyLevelDetailsButton").click(function () {
   
    var isNone = $("#CybotCookiebotDialogDetail").css("display");
    if (isNone == "none") {
        $("#CybotCookiebotDialogDetail").css("display", "block");
    }
    else if (isNone == "block") {
        $("#CybotCookiebotDialogDetail").css("display", "none");
    }
  

});


$("#iconWrapper").click(function () {
    
    $("#CybotCookiebotDialog").show();

})

$("#CybotCookiebotDialogDetailBodyContentTabsOverview").click(function () {
    
    var isSelected = $("#CybotCookiebotDialogDetailBodyContentTabsOverview").hasClass("CybotCookiebotDialogDetailBodyContentTabsItemSelected");
    var isNone = $("#CybotCookiebotDialogDetailBodyContentTextOverview").css("display");
    if (!isSelected) {
        UnSelectParentTabs();
        $("#CybotCookiebotDialogDetailBodyContentTabsOverview").addClass("CybotCookiebotDialogDetailBodyContentTabsItemSelected");
        if (isNone == "none") {
            $("#CybotCookiebotDialogDetailBodyContentTextOverview").css("display", "block");
        }
        else if (isNone == "block") {
            $("#CybotCookiebotDialogDetailBodyContentTextOverview").css("display", "none");
        }
        $("#CybotCookiebotDialogDetailBodyContentTabsAbout").removeClass("CybotCookiebotDialogDetailBodyContentTabsAbout");
        $("#CybotCookiebotDialogDetailBodyContentTextAbout").css("display", "none");
    }

})


$("#CybotCookiebotDialogDetailBodyContentTabsAbout").click(function () {
    
    var isSelected = $("#CybotCookiebotDialogDetailBodyContentTabsAbout").hasClass("CybotCookiebotDialogDetailBodyContentTabsItemSelected");
    var isNone = $("#CybotCookiebotDialogDetailBodyContentTextAbout").css("display");
    if (!isSelected) {
        UnSelectParentTabs();
        $("#CybotCookiebotDialogDetailBodyContentTabsAbout").addClass("CybotCookiebotDialogDetailBodyContentTabsItemSelected");
        if (isNone == "none") {
            $("#CybotCookiebotDialogDetailBodyContentTextAbout").css("display", "block");
        }
        else if (isNone == "block") {
            $("#CybotCookiebotDialogDetailBodyContentTextAbout").css("display", "none");
        }
        $("#CybotCookiebotDialogDetailBodyContentTabsOverview").removeClass("CybotCookiebotDialogDetailBodyContentTabsItemSelected");
        $("#CybotCookiebotDialogDetailBodyContentTextOverview").css("display", "none");
    }

})

function UnSelectParentTabs() {
    var parentTabl1 = $("#CybotCookiebotDialogDetailBodyContentTabsOverview").hasClass("CybotCookiebotDialogDetailBodyContentTabsItemSelected");
    var parentTabl2 = $("#CybotCookiebotDialogDetailBodyContentTabsAbout").hasClass("CybotCookiebotDialogDetailBodyContentTabsItemSelected");
    if (parentTabl1) {
        $("#CybotCookiebotDialogDetailBodyContentTabsOverview").removeClass("CybotCookiebotDialogDetailBodyContentTabsItemSelected");
        $("#CybotCookiebotDialogDetailBodyContentTabsOverview").addClass("CybotCookiebotDialogDetailBodyContentTabsItem");
    }
    if (parentTabl2) {
        $("#CybotCookiebotDialogDetailBodyContentTabsAbout").removeClass("CybotCookiebotDialogDetailBodyContentTabsItemSelected");
        $("#CybotCookiebotDialogDetailBodyContentTabsAbout").addClass("CybotCookiebotDialogDetailBodyContentTabsItem");
    }
}








$("#CybotCookiebotDialogDetailBodyContentCookieContainerNecessary").click(function () {
    
    var isNoneNecessary = $("#CybotCookiebotDialogDetailBodyContentCookieTabsNecessary").css("display");
    var isSelected = $("#CybotCookiebotDialogDetailBodyContentCookieContainerNecessary").hasClass("CybotCookiebotDialogDetailBodyContentCookieContainerTypesSelected");

    if (!isSelected) {
        UnselectOtherTabs()
        $("#CybotCookiebotDialogDetailBodyContentCookieContainerNecessary").addClass("CybotCookiebotDialogDetailBodyContentCookieContainerTypesSelected");
        if (isNoneNecessary == "none") {
            $("#CybotCookiebotDialogDetailBodyContentCookieTabsNecessary").css("display", "block");
        }
        else if (isNoneNecessary == "block") {
            $("#CybotCookiebotDialogDetailBodyContentCookieTabsNecessary").css("display", "none");
        }

        $("#CybotCookiebotDialogDetailBodyContentCookieTabsPreference").css("display", "none");
        $("#CybotCookiebotDialogDetailBodyContentCookieTabsStatistics").css("display", "none");
        $("#CybotCookiebotDialogDetailBodyContentCookieTabsAdvertising").css("display", "none");
        $("#CybotCookiebotDialogDetailBodyContentCookieTabsUnclassified").css("display", "none");
    }


});


$("#CybotCookiebotDialogDetailBodyContentCookieContainerPreference").click(function () {
   
    var isNonePreference = $("#CybotCookiebotDialogDetailBodyContentCookieTabsPreference").css("display");
    var isSelected = $("#CybotCookiebotDialogDetailBodyContentCookieContainerPreference").hasClass("CybotCookiebotDialogDetailBodyContentCookieContainerTypesSelected");

    if (!isSelected) {
        UnselectOtherTabs();
        $("#CybotCookiebotDialogDetailBodyContentCookieContainerPreference").addClass("CybotCookiebotDialogDetailBodyContentCookieContainerTypesSelected");
        if (isNonePreference == "none") {
            $("#CybotCookiebotDialogDetailBodyContentCookieTabsPreference").css("display", "block");
        }
        else if (isNonePreference == "block") {
            $("#CybotCookiebotDialogDetailBodyContentCookieTabsPreference").css("display", "none");
        } else {
            $("#CybotCookiebotDialogDetailBodyContentCookieTabsPreference").css("display", "none");
        }

        $("#CybotCookiebotDialogDetailBodyContentCookieTabsNecessary").css("display", "none");
        $("#CybotCookiebotDialogDetailBodyContentCookieTabsStatistics").css("display", "none");
        $("#CybotCookiebotDialogDetailBodyContentCookieTabsAdvertising").css("display", "none");
        $("#CybotCookiebotDialogDetailBodyContentCookieTabsUnclassified").css("display", "none");

    }

});



$("#CybotCookiebotDialogDetailBodyContentCookieContainerStatistics").click(function () {
   
    var isNoneStatistic = $("#CybotCookiebotDialogDetailBodyContentCookieTabsStatistics").css("display");
    var isSelected = $("#CybotCookiebotDialogDetailBodyContentCookieContainerStatistics").hasClass("CybotCookiebotDialogDetailBodyContentCookieContainerTypesSelected");
    if (!isSelected) {
        UnselectOtherTabs();
        $("#CybotCookiebotDialogDetailBodyContentCookieContainerStatistics").addClass("CybotCookiebotDialogDetailBodyContentCookieContainerTypesSelected");
        if (isNoneStatistic == "none") {
            $("#CybotCookiebotDialogDetailBodyContentCookieTabsStatistics").css("display", "block");
        }
        else if (isNoneStatistic == "block") {
            $("#CybotCookiebotDialogDetailBodyContentCookieTabsStatistics").css("display", "none");
        } else {
            $("#CybotCookiebotDialogDetailBodyContentCookieTabsStatistics").css("display", "none");
        }

        $("#CybotCookiebotDialogDetailBodyContentCookieTabsNecessary").css("display", "none");
        $("#CybotCookiebotDialogDetailBodyContentCookieTabsPreference").css("display", "none");
        $("#CybotCookiebotDialogDetailBodyContentCookieTabsAdvertising").css("display", "none");
        $("#CybotCookiebotDialogDetailBodyContentCookieTabsUnclassified").css("display", "none");
    }


});


$("#CybotCookiebotDialogDetailBodyContentCookieContainerAdvertising").click(function () {
   
    var isNoneAdvertising = $("#CybotCookiebotDialogDetailBodyContentCookieTabsAdvertising").css("display");
    var isSelected = $("#CybotCookiebotDialogDetailBodyContentCookieContainerAdvertising").hasClass("CybotCookiebotDialogDetailBodyContentCookieContainerTypesSelected");
    if (!isSelected) {
        UnselectOtherTabs();
        $("#CybotCookiebotDialogDetailBodyContentCookieContainerAdvertising").addClass("CybotCookiebotDialogDetailBodyContentCookieContainerTypesSelected");
        if (isNoneAdvertising == "none") {
            $("#CybotCookiebotDialogDetailBodyContentCookieTabsAdvertising").css("display", "block");
        }
        else if (isNoneAdvertising == "block") {
            $("#CybotCookiebotDialogDetailBodyContentCookieTabsAdvertising").css("display", "none");
        } else {
            $("#CybotCookiebotDialogDetailBodyContentCookieTabsAdvertising").css("display", "none");
        }

        $("#CybotCookiebotDialogDetailBodyContentCookieTabsNecessary").css("display", "none");
        $("#CybotCookiebotDialogDetailBodyContentCookieTabsPreference").css("display", "none");
        $("#CybotCookiebotDialogDetailBodyContentCookieTabsStatistics").css("display", "none");
        $("#CybotCookiebotDialogDetailBodyContentCookieTabsUnclassified").css("display", "none");
    }


});


$("#CybotCookiebotDialogDetailBodyContentCookieContainerUnclassified").click(function () {
   
    var isNoneAdvertising = $("#CybotCookiebotDialogDetailBodyContentCookieTabsUnclassified").css("display");
    var isSelected = $("#CybotCookiebotDialogDetailBodyContentCookieContainerUnclassified").hasClass("CybotCookiebotDialogDetailBodyContentCookieContainerTypesSelected");

    if (!isSelected) {
        UnselectOtherTabs();
        $("#CybotCookiebotDialogDetailBodyContentCookieContainerUnclassified").addClass("CybotCookiebotDialogDetailBodyContentCookieContainerTypesSelected");
        if (isNoneAdvertising == "none") {
            $("#CybotCookiebotDialogDetailBodyContentCookieTabsUnclassified").css("display", "block");
        }
        else if (isNoneAdvertising == "block") {
            $("#CybotCookiebotDialogDetailBodyContentCookieTabsUnclassified").css("display", "none");
        } else {
            $("#CybotCookiebotDialogDetailBodyContentCookieTabsUnclassified").css("display", "none");
        }

        $("#CybotCookiebotDialogDetailBodyContentCookieTabsNecessary").css("display", "none");
        $("#CybotCookiebotDialogDetailBodyContentCookieTabsPreference").css("display", "none");
        $("#CybotCookiebotDialogDetailBodyContentCookieTabsStatistics").css("display", "none");
        $("#CybotCookiebotDialogDetailBodyContentCookieTabsAdvertising").css("display", "none");
    }


});

function UnselectOtherTabs() {
    var tab1 = $("#CybotCookiebotDialogDetailBodyContentCookieContainerNecessary").hasClass("CybotCookiebotDialogDetailBodyContentCookieContainerTypesSelected");
    var tab2 = $("#CybotCookiebotDialogDetailBodyContentCookieContainerPreference").hasClass("CybotCookiebotDialogDetailBodyContentCookieContainerTypesSelected");
    var tab3 = $("#CybotCookiebotDialogDetailBodyContentCookieContainerStatistics").hasClass("CybotCookiebotDialogDetailBodyContentCookieContainerTypesSelected");
    var tab4 = $("#CybotCookiebotDialogDetailBodyContentCookieContainerAdvertising").hasClass("CybotCookiebotDialogDetailBodyContentCookieContainerTypesSelected");
    var tab5 = $("#CybotCookiebotDialogDetailBodyContentCookieContainerUnclassified").hasClass("CybotCookiebotDialogDetailBodyContentCookieContainerTypesSelected");

    if (tab1) {
        $("#CybotCookiebotDialogDetailBodyContentCookieContainerNecessary").removeClass("CybotCookiebotDialogDetailBodyContentCookieContainerTypesSelected");
        $("#CybotCookiebotDialogDetailBodyContentCookieContainerNecessary").addClass("CybotCookiebotDialogDetailBodyContentCookieContainerTypes");
    }
    if (tab2) {
        $("#CybotCookiebotDialogDetailBodyContentCookieContainerPreference").removeClass("CybotCookiebotDialogDetailBodyContentCookieContainerTypesSelected");
        $("#CybotCookiebotDialogDetailBodyContentCookieContainerPreference").addClass("CybotCookiebotDialogDetailBodyContentCookieContainerTypes");
    }
    if (tab3) {
        $("#CybotCookiebotDialogDetailBodyContentCookieContainerStatistics").removeClass("CybotCookiebotDialogDetailBodyContentCookieContainerTypesSelected");
        $("#CybotCookiebotDialogDetailBodyContentCookieContainerStatistics").addClass("CybotCookiebotDialogDetailBodyContentCookieContainerTypes");
    }
    if (tab4) {
        $("#CybotCookiebotDialogDetailBodyContentCookieContainerAdvertising").removeClass("CybotCookiebotDialogDetailBodyContentCookieContainerTypesSelected");
        $("#CybotCookiebotDialogDetailBodyContentCookieContainerAdvertising").addClass("CybotCookiebotDialogDetailBodyContentCookieContainerTypes");
    }
    if (tab5) {
        $("#CybotCookiebotDialogDetailBodyContentCookieContainerUnclassified").removeClass("CybotCookiebotDialogDetailBodyContentCookieContainerTypesSelected");
        $("#CybotCookiebotDialogDetailBodyContentCookieContainerUnclassified").addClass("CybotCookiebotDialogDetailBodyContentCookieContainerTypes");
    }
}

