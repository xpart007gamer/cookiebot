﻿

//Allow All Cookies
$("#CybotCookiebotDialogBodyLevelButtonLevelOptinAllowAll").click(function () {
    



    var Necessary = $("#CybotCookiebotDialogBodyLevelButtonNecessary").prop("checked", true);
    var Preference = $("#CybotCookiebotDialogBodyLevelButtonPreferences").prop("checked", true);
    var Statistic = $("#CybotCookiebotDialogBodyLevelButtonStatistics").prop("checked", true);
    var Marketing = $("#CybotCookiebotDialogBodyLevelButtonMarketing").prop("checked", true);

    var Necessary = Necessary == false ? "unchecked" : "checked";
    var Preference = Preference == false ? "unchecked" : "checked";
    var Statistic = Statistic == false ? "unchecked" : "checked";
    var Marketing = Marketing == false ? "unchecked" : "checked";

    SaveCookieSettings(Necessary, Preference, Statistic, Marketing);
    $("#CybotCookiebotDialog").hide();

});

//Allow Sections
$("#CybotCookiebotDialogBodyLevelButtonLevelOptinAllowallSelection").click(function () {
    


    var Necessary = $("#CybotCookiebotDialogBodyLevelButtonNecessary").prop("checked");
    var Preference = $("#CybotCookiebotDialogBodyLevelButtonPreferences").prop("checked");
    var Statistic = $("#CybotCookiebotDialogBodyLevelButtonStatistics").prop("checked");
    var Marketing = $("#CybotCookiebotDialogBodyLevelButtonMarketing").prop("checked");

    var Necessary = Necessary == false ? "unchecked" : "checked";
    var Preference = Preference == false ? "unchecked" : "checked";
    var Statistic = Statistic == false ? "unchecked" : "checked";
    var Marketing = Marketing == false ? "unchecked" : "checked";
   

    SaveCookieSettings(Necessary, Preference, Statistic, Marketing);
    $("#CybotCookiebotDialog").hide();

   

});
//Use Necessary Cookies Only
$("#CybotCookiebotDialogBodyLevelButtonLevelOptinDeclineAll").click(function () {
    


    var Necessary = $("#CybotCookiebotDialogBodyLevelButtonNecessary").prop("checked",);
    var Preference = $("#CybotCookiebotDialogBodyLevelButtonPreferences").prop("checked", false);
    var Statistic = $("#CybotCookiebotDialogBodyLevelButtonStatistics").prop("checked", false);
    var Marketing = $("#CybotCookiebotDialogBodyLevelButtonMarketing").prop("checked", false);

    var Necessary = Necessary == false ? "unchecked" : "checked";
    var Preference = "unchecked";
    var Statistic = "unchecked";
    var Marketing = "unchecked";
   

    SaveCookieSettings(Necessary, Preference, Statistic, Marketing);
    $("#CybotCookiebotDialog").hide();

   

});




function SaveCookieSettings(Necessary, Preference, Statistic, Marketing) {
    
    var Settings = {
        Necessary: Necessary,
        Preference: Preference,
        Statistic: Statistic,
        Marketing: Marketing,
    };



    if (localStorage.getItem("Settings") != null) {
        localStorage.clear();
        var settingsSerialized = JSON.stringify(Settings);
        localStorage.setItem("Settings", settingsSerialized);
    } else {
        var settingsSerialized = JSON.stringify(Settings);
        localStorage.setItem("Settings", settingsSerialized);
    }


}