﻿using CookieBot.Models;
using Microsoft.AspNet.Identity.EntityFramework;
using MySql.Data.EntityFramework;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace CookieBot.Context
{
    [DbConfigurationType(typeof(MySqlEFConfiguration))]
    public class CookieContext : IdentityDbContext<IdentityUser>
    {
        public CookieContext()
            //Reference the name of your connection string ( WebAppCon )  
            : base("connectionstring") { }

        public DbSet<Domain> Domains { get; set; }
        public DbSet<Dialog> Dialogs { get; set; }
        public DbSet<Content> Contents { get; set; }
        public DbSet<Script> Scripts { get; set; }
        public DbSet<Users> AppUsers { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);
        }
    }


}