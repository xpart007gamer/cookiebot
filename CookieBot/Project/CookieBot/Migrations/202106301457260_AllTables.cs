﻿namespace CookieBot.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AllTables : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Contents",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        DialogHeading = c.String(unicode: false),
                        Dialoguody = c.String(unicode: false),
                        Language = c.String(unicode: false),
                        TextInAllowAllButton = c.String(unicode: false),
                        TextInAllowSectionButton = c.String(unicode: false),
                        TextInShowSectionButton = c.String(unicode: false),
                        TextInHideDetailsButton = c.String(unicode: false),
                        GeneralCookieIntroduction = c.String(unicode: false),
                        NecessaryCategoryTitle = c.String(unicode: false),
                        NecessaryCookieIntroduction = c.String(unicode: false),
                        PreferenceCategoryTitle = c.String(unicode: false),
                        PreferenceCookieIntroduction = c.String(unicode: false),
                        StatisticCategoryTitle = c.String(unicode: false),
                        StatisticCookieIntroduction = c.String(unicode: false),
                        MarketingCategoryTitle = c.String(unicode: false),
                        MarketingCookieIntroduction = c.String(unicode: false),
                        UnclassifiedCategoryTitle = c.String(unicode: false),
                        UnclassifiedCookieIntroduction = c.String(unicode: false),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.Dialogs",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        TemplateType = c.Int(nullable: false),
                        MethodType = c.Int(nullable: false),
                        PositionType = c.Int(nullable: false),
                        ButtonType = c.Int(nullable: false),
                        Preference = c.Boolean(),
                        Statistics = c.Boolean(),
                        Marketing = c.Boolean(),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.Scripts",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        ScriptString = c.String(unicode: false),
                    })
                .PrimaryKey(t => t.Id);
            
        }
        
        public override void Down()
        {
            DropTable("dbo.Scripts");
            DropTable("dbo.Dialogs");
            DropTable("dbo.Contents");
        }
    }
}
