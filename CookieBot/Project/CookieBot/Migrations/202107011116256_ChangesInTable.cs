﻿namespace CookieBot.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class ChangesInTable : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.Dialogs", "Preference", c => c.Boolean(nullable: false));
            AlterColumn("dbo.Dialogs", "Statistics", c => c.Boolean(nullable: false));
            AlterColumn("dbo.Dialogs", "Marketing", c => c.Boolean(nullable: false));
        }
        
        public override void Down()
        {
            AlterColumn("dbo.Dialogs", "Marketing", c => c.Boolean());
            AlterColumn("dbo.Dialogs", "Statistics", c => c.Boolean());
            AlterColumn("dbo.Dialogs", "Preference", c => c.Boolean());
        }
    }
}
