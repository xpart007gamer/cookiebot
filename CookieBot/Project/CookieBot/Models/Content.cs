﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace CookieBot.Models
{
    public class Content
    {
        [Key]
        public int  Id { get; set; }

        public string DialogHeading { get; set; }

        [AllowHtml]
        public string DialogBody { get; set; }
        public string Language { get; set; }
        public string TextInDeclineButton { get; set; }
        public string TextInOkButton { get; set; }
        public string TextInAllowAllButton { get; set; }
        public string TextInAllowSectionButton { get; set; }
        public string TextInShowSectionButton { get; set; }     
        public string TextInHideDetailsButton { get; set; }
        [AllowHtml]
        public string GeneralCookieIntroduction { get; set; }
        public string NecessaryCategoryTitle { get; set; }
        [AllowHtml]
        public string NecessaryCookieIntroduction { get; set; }
        public string PreferenceCategoryTitle { get; set; }
        [AllowHtml]
        public string PreferenceCookieIntroduction { get; set; }
        public string StatisticCategoryTitle { get; set; }
        [AllowHtml]
        public string StatisticCookieIntroduction { get; set; }
        public string MarketingCategoryTitle { get; set; }
        [AllowHtml]
        public string MarketingCookieIntroduction { get; set; }

        public string UnclassifiedCategoryTitle { get; set; }
        [AllowHtml]
        public string UnclassifiedCookieIntroduction { get; set; }


        public int UserId { get; set; }


    }
}