﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace CookieBot.Models
{
    public class Script
    {
        [Key]
        public int Id { get; set; }

        [AllowHtml]
        public string ScriptString { get; set; }

        public int UserId { get; set; }
    }
}