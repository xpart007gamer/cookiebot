﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace CookieBot.Models
{
    public class Users : BaseEntity
    {
        [Key]
        public int Id { get; set; }



        //[Required(ErrorMessage = "Email is required.")]
        [DataType(DataType.EmailAddress, ErrorMessage = "Please Provide a Valid Email Address")]
        [Display(Name = "EmailOfUser", ResourceType = typeof(UserRegistrationStrings))]
        public string Email { get; set; }

        [Required(ErrorMessage = "Full Name is required.")]
        [Display(Name = "FullNameOfUser", ResourceType = typeof(UserRegistrationStrings))]
        public string FullName { get; set; }

        [Required(ErrorMessage = "Password is required.")]
        [Display(Name = "PasswordOfUser", ResourceType = typeof(UserRegistrationStrings))]
        public string Password { get; set; }



        [Display(Name = "PhoneNumberOfUser", ResourceType = typeof(UserRegistrationStrings))]
        public string PhoneNumber { get; set; }

    }
}