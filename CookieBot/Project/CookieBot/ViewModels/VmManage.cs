﻿using CookieBot.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CookieBot.ViewModels
{
    public class VmManage
    {
        public string Test { get; set; }
        public Content Content { get; set; }
        public Dialog Dialog { get; set; }
        public Domain Domain { get; set; }
        public Script Script { get; set; }
    }
}