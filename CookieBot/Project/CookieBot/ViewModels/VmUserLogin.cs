﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace CookieBot.ViewModels
{
    public class VmUserLogin
    {
        public int Id { get; set; }
        [Required(ErrorMessage = "User Name is required.")]
        [Display(Name = "FullNameOfUser", ResourceType = typeof(UserRegistrationStrings))]
        public string FullName { get; set; }

        [Required(ErrorMessage = "Password is required.")]
        [Display(Name = "PasswordOfUser", ResourceType = typeof(UserRegistrationStrings))]
        public string Password { get; set; }
    }
}