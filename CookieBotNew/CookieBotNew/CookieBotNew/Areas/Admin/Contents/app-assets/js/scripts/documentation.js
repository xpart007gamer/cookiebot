/*=========================================================================================
    File Name: documentation.js
    Description: Theme documentation js file
    ----------------------------------------------------------------------------------------
    Item Name: Frest HTML Admin Template
    Version: 1.0
    Author: radiantsoftbd
    Author URL: http://www.themeforest.net/user/radiantsoftbd
==========================================================================================*/

$(document).ready(function(){
   $('body').scrollspy({ target: '#sidebar-page-navigation' });
});
