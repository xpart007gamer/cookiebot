﻿function PhotoReadURL(input) {
    if (input.files && input.files[0]) {
        var reader = new FileReader();
        reader.onload = function (e) {
            $('#ExistingPhoto').attr('src', e.target.result);
        }
        reader.readAsDataURL(input.files[0]);
    }
}
$("#Photo").change(function () {
    PhotoReadURL(this);
});
$("#Logo").change(function () {
    PhotoReadURL(this);
});
$("#Attachment").change(function () {
    PhotoReadURL(this);
});