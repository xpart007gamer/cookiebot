﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Web;

namespace CookieBot.Areas.Admin.Services
{
    public class SecureGuidGenerator
    {
        [ThreadStatic]
        static RandomNumberGenerator rng;

        public static Guid GetNext()
        {
            if (rng == null) rng = RandomNumberGenerator.Create();
            var bytes = new byte[16];
            rng.GetBytes(bytes);
            return new Guid(bytes);
        }
    }
}