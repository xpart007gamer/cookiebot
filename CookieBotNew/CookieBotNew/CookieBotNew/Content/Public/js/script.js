$(".dropdown-toggle").click(function() {
    $(this).toggleClass("crosser");
});

// Mega Menu
$(".menu-opener").click(function() {
    $(".mega-menu").toggleClass("open");
});
$(".menu-closer").click(function() {
    $(".mega-menu").toggleClass("open");
});

// Add class on scroll
$(window).scroll(function() {
    var scroll = $(window).scrollTop();
    if (scroll >= 600) {
        $(".top-head").addClass("fixed-header");
    } else {
        $(".top-head").removeClass("fixed-header");
    }
});