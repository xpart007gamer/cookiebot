﻿using System;
using System.Globalization;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.Owin.Security;
using CookieBotNew.Models;

namespace CookieBot.Controllers
{
    public class HomeController : Controller
    {

        CookieContext _context = new CookieContext();
     
        public ActionResult Index()
        {

            var model = new Users();

            return View(model);
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }
        
        public ActionResult Cookie()
        {
            var domain = _context.Domains.FirstOrDefault();
            var content = _context.Contents.FirstOrDefault();
            var dialog = _context.Dialogs.FirstOrDefault();
            var script = _context.Scripts.FirstOrDefault();
            var model = new VmManage();
            if (domain != null && content != null && dialog != null && script != null)
            {
                model.Content = content;
                model.Domain = domain;
                model.Dialog = dialog;
                model.Script = script;
            }
            return View(model);
        }

        public ActionResult CookiePreview()
        {
            var domain = _context.Domains.FirstOrDefault();
            var content = _context.Contents.FirstOrDefault();
            var dialog = _context.Dialogs.FirstOrDefault();
            var script = _context.Scripts.FirstOrDefault();
            var model = new VmManage();
            if (domain != null && content != null && dialog != null && script != null)
            {
                model.Content = content;
                model.Domain = domain;
                model.Dialog = dialog;
                model.Script = script;
            }
            return View(model);
        }
    }
}