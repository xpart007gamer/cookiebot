﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CookieBot.Enums
{
    public class EnumSets
    {
        public  enum SchemeType
        {
            Monthly = 0,
        }

        public enum TemplateType 
        { 
            Classic_Design = 0,
            //Custom = 1,
        }
        public enum MethodType
        {
            Implied_Consent = 0,
            Explicit_Consent = 1,
        }
        public enum PositionType
        {
            Top = 0,
            Bottom = 1,
            Push_Down = 2,
            Slide_Down = 3,
            Slide_Up = 4,
            Pop_Up = 5,
            Overlay = 6,
        }
        public enum ButtonType
        {
            Decline_Only = 0,
            Accept_Only = 1,
            AcceptOrDecline = 2,
            Multilevel = 3,
            Inline_Multilevel = 4,
            DoNotSell = 5,
          
        }
        public enum Button
        {
            RejectAll_Selection_AllowAll = 0,
            AllowAll_Selection= 1,
            Ok = 2,
         
        }
    }
}