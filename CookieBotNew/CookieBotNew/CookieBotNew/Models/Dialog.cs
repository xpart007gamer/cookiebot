﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using static CookieBot.Enums.EnumSets;

namespace CookieBot.Models
{
    public class Dialog
    {
        [Key]
        public int Id { get; set; }
        public TemplateType TemplateType { get; set; }
        public MethodType MethodType { get; set; }
        public PositionType PositionType { get; set; }
        public Button Button { get; set; }
        public ButtonType ButtonType { get; set; }

        public bool Preference { get; set; }
        public bool Statistics { get; set; }
        public bool Marketing { get; set; }
        public bool PageScroll { get; set; }
        public bool PageRefresh { get; set; }
        public bool DisplayBanner { get; set; }

        public int UserId { get; set; }
    }
}