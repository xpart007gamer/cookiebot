﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using static CookieBot.Enums.EnumSets;

namespace CookieBot.Models
{
    public class Domain
    {
        [Key]
        public int Id { get; set; }
        public string DomainName { get; set; }
        public SchemeType SchemeType { get; set; }

        public int UserId { get; set; }
    }
}