﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(CookieBotNew.Startup))]
namespace CookieBotNew
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
